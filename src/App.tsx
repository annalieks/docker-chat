import React from 'react';
import AppHeader from './components/AppHeader';
import Chat from './components/Chat';

function App() {
  return (
    <div>
      <AppHeader />
      <Chat />
    </div>
  );
}

export default App;
